## Install app

On Mac or Linux:
```
./gradlew clean installLocalDebug
```
On Windows:
```
gradlew clean installLocalDebug
```
## Running the Instrumented tests

On Mac or Linux:
```
./gradlew clean connectedLocalDebugAndroidTest
```
On Windows:
```
gradlew clean connectedLocalDebugAndroidTest

```

## Third party libraries used

* Dagger 2
* RxJava 2
* Retrofit 2
* okHttp 3
* Espresso
* JUnit