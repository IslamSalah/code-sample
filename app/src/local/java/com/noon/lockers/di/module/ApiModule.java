package com.noon.lockers.di.module;

import dagger.Module;

/**
 * Created by Islam Salah on 2019-05-19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * Empty module, corresponding to its counterpart in other flavors, to allow Dagger dependency
 * graph construction
 */
@Module
public class ApiModule {

}
