package com.noon.lockers.data.network;

import android.content.Context;

import androidx.annotation.StringRes;

import com.noon.lockers.R;
import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.data.network.model.response.LockerBox;
import com.noon.lockers.testutils.EspressoIdlingResource;

import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * This implementation carries on no logic. It's used to test, manually or using automated tests,
 * the integration between the backend layer and app logic.
 */
public class ApiHelperImp implements ApiHelper {

    private static final int FAKE_DELAY_PERIOD = 3; //seconds

    private static final String[] VALID_PACKAGE_IDS = {"10010210055055", "100102100551343"};
    private static final String[] VALID_DRIVER_TOKENS = {"10010210055134892", "10010210055030371"};

    private Context mContext;

    @Inject
    public ApiHelperImp(Context context) {
        mContext = context;
    }

    @Override
    public Single<Locker> registerLocker(String secretKey) {
        return synchronizeEspresso(Single.fromCallable(() -> {
            mimicDelay(FAKE_DELAY_PERIOD);
            return new Locker("Id", "Name");
        }));
    }

    private <T> Single<T> synchronizeEspresso(Single<T> single) {
        return single.doOnSubscribe(d -> EspressoIdlingResource.INSTANCE.increment())
                .doFinally(EspressoIdlingResource.INSTANCE::decrement);
    }

    @Override
    public Single<LockerBox> authenticatePickupCode(String lockerId, String pickupCode) {
        return synchronizeEspresso(Single.fromCallable(() -> {
            mimicDelay(FAKE_DELAY_PERIOD);

            int boxNumber = mapPickupCodeToBoxNumber(pickupCode);
            if (boxNumber == -1) {
                throw new Exception(getString(R.string.invalid_pick_up_code_message));
            } else {
                return new LockerBox(boxNumber);
            }
        }));
    }

    private int mapPickupCodeToBoxNumber(String pickupCode) {
        int lockerBoxNumber;

        try {
            lockerBoxNumber = Integer.parseInt(pickupCode);
        } catch (Exception e) {
            lockerBoxNumber = -1;
        }

        return lockerBoxNumber > 0 ? lockerBoxNumber : -1;
    }

    @Override
    public Single<Driver> authenticateDriverQrCode(String driverAccessToken) {
        return synchronizeEspresso(Single.fromCallable(() -> {
            mimicDelay(FAKE_DELAY_PERIOD);

            if (!isIdContainsValidId(driverAccessToken, VALID_DRIVER_TOKENS)) {
                throw new Exception(getString(R.string.unauthorized_message));
            } else {
                boolean isPutAwayAction = new Random().nextBoolean();
                String action = getString(isPutAwayAction ? R.string.put_away_message : R.string.harvest_message);
                return new Driver("", action, "", "");
            }
        }));
    }

    @Override
    public Single<LockerBox> scanPackageQrCode(String lockerId, String driverAccessToken,
                                               String packageQrCode, boolean isPutAway) {
        return generateRandomBoxNumber(packageQrCode);
    }

    private Single<LockerBox> generateRandomBoxNumber(String packageId) {
        return synchronizeEspresso(Single.fromCallable(() -> {
            mimicDelay(FAKE_DELAY_PERIOD);

            if (!isIdContainsValidId(packageId, VALID_PACKAGE_IDS)) {
                throw new Exception(getString(R.string.invalid_package_qr_code_message));
            } else {
                Random random = new Random();
                int boxNumber = 1 + random.nextInt(15);
                return new LockerBox(boxNumber);
            }
        }));
    }

    private boolean isIdContainsValidId(String id, String[] validIds) {
        for (String validCode : validIds) {
            if (id.contains(validCode)) return true;
        }
        return false;
    }

    private String getString(@StringRes int resource) {
        return mContext.getString(resource);
    }

    private void mimicDelay(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }
}
