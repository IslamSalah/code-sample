package com.noon.lockers.data.network;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.noon.lockers.R;
import com.noon.lockers.data.network.model.request.LockerRegistrationRequest;
import com.noon.lockers.data.network.model.request.Package;
import com.noon.lockers.data.network.model.request.PickupRequest;
import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.data.network.model.response.LockerBox;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ApiHelperImp implements ApiHelper {

    @Inject
    ServerClock mServerClock;

    @Inject
    ApiService mApiService;

    @Inject
    Context mContext;

    private Single mOfflineSource = Single.fromCallable(() -> {
        String errorMessage = mContext.getString(R.string.no_internet_message);
        throw new NetworkErrorException(errorMessage);
    });

    @Inject
    public ApiHelperImp() {
        /* Required by dagger2 framework */
    }

    @Override
    public Single<Locker> registerLocker(String secretKey) {
        if (!isThereInternetConnection()) return mOfflineSource;
        LockerRegistrationRequest registrationRequest = new LockerRegistrationRequest(secretKey);
        return mApiService.registerLocker(registrationRequest);
    }

    private boolean isThereInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public Single<LockerBox> authenticatePickupCode(String lockerId, String pickupCode) {
        if (!isThereInternetConnection()) return mOfflineSource;
        String currentTimestamp = mServerClock.getCurrentTimestamp();
        PickupRequest pickupRequest = new PickupRequest(pickupCode, currentTimestamp);
        return mApiService.pickupPackage(lockerId, pickupRequest);
    }

    @Override
    public Single<Driver> authenticateDriverQrCode(String driverAccessToken) {
        if (!isThereInternetConnection()) return mOfflineSource;
        return mApiService.authenticateDriver(driverAccessToken);
    }

    @Override
    public Single<LockerBox> scanPackageQrCode(String lockerId, String driverAccessToken,
                                               String packageQrCode, boolean isPutAway) {
        String currentTimestamp = mServerClock.getCurrentTimestamp();
        Package scannedPackage = new Package(packageQrCode, currentTimestamp);
        return scanPackageQrCode(lockerId, driverAccessToken, scannedPackage, isPutAway);
    }

    private Single<LockerBox> scanPackageQrCode(String lockerId, String driverAccessToken,
                                                Package scannedPackage, boolean isPutAway) {
        if (!isThereInternetConnection()) return mOfflineSource;
        if (isPutAway) {
            return mApiService.putAway(lockerId, driverAccessToken, scannedPackage);
        } else {
            return mApiService.harvest(lockerId, driverAccessToken, scannedPackage);
        }
    }
}
