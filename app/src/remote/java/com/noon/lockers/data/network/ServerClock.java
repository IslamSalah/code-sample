package com.noon.lockers.data.network;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.inject.Inject;

/**
 * Created by Islam Salah on 2019-05-19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ServerClock {

    private static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXX";
    private static final String SERVER_TIME_ZONE = "Etc/UTC";

    @Inject
    public ServerClock() {
        /* Required by dagger2 framework */
    }

    public String getCurrentTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone(SERVER_TIME_ZONE));
        return dateFormat.format(new Date().getTime());
    }
}
