package com.noon.lockers.locker;

import android.content.Context;

import javax.inject.Inject;

/**
 * Created by Islam Salah on 2019-05-06.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class FakeLockerMachine implements LockerMachine {

    private OnScanListener mOnScanListener;
    private OnScanListener mOldOnScannerListener; // cashes old listener
    private boolean mIsScannerOn = true;

    @Inject
    FakeLockerMachine() {
        /* Required by dagger2 framework */
    }

    @Override
    public boolean bind(Context context) {
        return true;
    }

    @Override
    public void unBind() {

    }

    @Override
    public boolean openBox(int boxNumber) {
        return true;
    }

    @Override
    public boolean isOpenedBox(int boxNumber) {
        return false;
    }

    @Override
    public boolean isClosedBox(int boxNumber) {
        return false;
    }

    @Override
    public void turnOnScanner(boolean isOn) {
        if (mIsScannerOn == isOn) return;
        mIsScannerOn = isOn;
        mOldOnScannerListener = isOn ? mOldOnScannerListener : mOnScanListener;
        mOnScanListener = isOn ? mOldOnScannerListener : null;
    }

    @Override
    public void setOnScanListener(OnScanListener listener) {
        mOnScanListener = listener;
    }

    @Override
    public void setOnCloseBoxListener(OnCloseBoxListener listener) {
    }

    public void simulateScanningAction(String scannedCode) {
        if (mOnScanListener == null) return;
        mOnScanListener.onScan(scannedCode);
    }
}
