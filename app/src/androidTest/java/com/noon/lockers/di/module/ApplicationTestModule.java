package com.noon.lockers.di.module;

import android.app.Application;
import android.content.Context;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.DataManagerImp;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.data.ErrorHandlerImp;
import com.noon.lockers.data.network.ApiHelper;
import com.noon.lockers.data.network.ApiHelperImp;
import com.noon.lockers.data.preferences.FakePreferencesHelper;
import com.noon.lockers.data.preferences.PreferencesHelper;
import com.noon.lockers.locker.FakeLockerMachine;
import com.noon.lockers.locker.LockerMachine;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Islam Salah on 2019-05-06.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

@Module
public class ApplicationTestModule {

    private Application mApplication;

    public ApplicationTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ErrorHandler provideErrorHandler(ErrorHandlerImp errorHandler) {
        return errorHandler;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(ApiHelperImp apiHelper) {
        return apiHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(DataManagerImp dataManager) {
        return dataManager;
    }

    /*********** FAKES ***********/

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(FakePreferencesHelper preferencesHelper) {
        return preferencesHelper;
    }

    @Provides
    @Singleton
    LockerMachine provideLockerMachine(FakeLockerMachine lockerMachine) {
        return lockerMachine;
    }
}
