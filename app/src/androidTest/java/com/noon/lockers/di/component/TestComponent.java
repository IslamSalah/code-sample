package com.noon.lockers.di.component;

import com.noon.lockers.di.module.ApplicationModule;
import com.noon.lockers.di.module.ApplicationTestModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Islam Salah on 2019-05-06.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {
}
