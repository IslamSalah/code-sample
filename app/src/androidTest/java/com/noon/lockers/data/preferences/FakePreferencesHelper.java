package com.noon.lockers.data.preferences;

import javax.inject.Inject;

/**
 * Created by Islam Salah on 2019-05-06.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class FakePreferencesHelper implements PreferencesHelper {

    @Inject
    FakePreferencesHelper() {
        /* Required by dagger2 framework */
    }

    @Override
    public String getLockerId() {
        return "fake id";
    }

    @Override
    public void setLockerId(String lockerId) {

    }

    @Override
    public String getLockerName() {
        return "fake name";
    }

    @Override
    public void setLockerName(String name) {

    }
}
