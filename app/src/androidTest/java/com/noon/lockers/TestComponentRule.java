package com.noon.lockers;

import android.content.Context;

import com.noon.lockers.di.component.DaggerTestComponent;
import com.noon.lockers.di.component.TestComponent;
import com.noon.lockers.di.module.ApplicationTestModule;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Created by Islam Salah on 2019-05-06.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class TestComponentRule implements TestRule {

    private Context mContext;
    private TestComponent mTestComponent;

    public TestComponentRule(Context context) {
        mContext = context;
        mTestComponent = DaggerTestComponent.builder()
                .applicationTestModule(new ApplicationTestModule(getApplication()))
                .build();
    }

    private LockersApplication getApplication() {
        return mContext == null ? null : (LockersApplication) mContext.getApplicationContext();
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                LockersApplication application = getApplication();
                application.setApplicationComponent(mTestComponent);
                base.evaluate();
            }
        };
    }

    public TestComponent getTestComponent() {
        return mTestComponent;
    }
}
