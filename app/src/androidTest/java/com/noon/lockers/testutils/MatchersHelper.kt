package com.noon.lockers.testutils

import android.view.View
import android.widget.TextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import org.hamcrest.Matcher

/**
 * Created by Islam Salah on 2019-05-09.
 *
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

fun getText(matcher: Matcher<View>): String? {

    var text: String? = null

    onView(matcher).perform(object : ViewAction {
        override fun getDescription() = "getting text from TextView"

        override fun getConstraints() = isAssignableFrom(TextView::class.java)

        override fun perform(uiController: UiController?, view: View?) {
            val textView = view as TextView
            text = textView.text.toString()
        }
    })

    return text
}