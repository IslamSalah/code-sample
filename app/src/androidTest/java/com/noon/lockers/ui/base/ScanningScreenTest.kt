package com.noon.lockers.ui.base

import android.app.Activity
import com.noon.lockers.locker.FakeLockerMachine
import com.noon.lockers.locker.LockerMachine

/**
 * Created by Islam Salah on 2019-05-09.
 *
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
abstract class ScanningScreenTest<T : Activity> : BaseScreenTest<T>() {

    /**
     * Simulates the real scenario by running on a background thread
     *
     * @see LockerMachine#setOnScanListener
     */
    protected fun simulateScanningAction(scannedCode: String) {
        val lockerMachine = getTestComponent().lockerMachine as? FakeLockerMachine ?: return
        Thread { lockerMachine.simulateScanningAction(scannedCode) }.run()
    }
}