package com.noon.lockers.ui.base

import android.app.Activity
import androidx.test.espresso.IdlingRegistry
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.noon.lockers.TestComponentRule
import com.noon.lockers.testutils.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.RuleChain
import org.junit.rules.TestRule

/**
 * Created by Islam Salah on 2019-05-07.
 *
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
abstract class BaseScreenTest<T : Activity> {

    private val mContext = InstrumentationRegistry.getInstrumentation().targetContext
    private val mTestRule = TestComponentRule(mContext)
    private val mActivityRule = ActivityTestRule(getActivityClass())

    @Rule
    @JvmField // The outer rule injects Dagger test dependencies while the inner launches activity
    val chain: TestRule = RuleChain.outerRule(mTestRule).around(mActivityRule)

    protected abstract fun getActivityClass(): Class<T>

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance()
                .register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unRegisterIdlingResource() {
        IdlingRegistry.getInstance()
                .unregister(EspressoIdlingResource.countingIdlingResource)
    }

    protected fun getTestComponent() = mTestRule.testComponent
}