package com.noon.lockers.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.getIntents
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.truth.content.IntentSubject.assertThat
import com.noon.lockers.R
import com.noon.lockers.testutils.getText
import com.noon.lockers.ui.authentication.AuthenticationActivity
import com.noon.lockers.ui.base.ScanningScreenTest
import com.noon.lockers.ui.driveropenlockerbox.DriverOpenLockerBoxActivity
import com.noon.lockers.ui.useropenlockerbox.UserOpenLockerBoxActivity
import junit.framework.TestCase.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Islam Salah on 2019-05-07.
 *
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * UI tests for the authentication screen used by both a customer and a driver
 */
@RunWith(AndroidJUnit4::class)
class AuthenticationScreenTest : ScanningScreenTest<AuthenticationActivity>() {

    private val VALID_PICKUP_CODE = "1"
    private val PICKUP_CODE_TO_LOCKER_BOX = hashMapOf("1" to 1)

    private val VALID_AUTH_CODE = "NE10010210055134892"
    private val INVALID_AUTH_CODE = "invalid code"
    private val SCAN_ATTEMPTS_COUNT = 1000

    override fun getActivityClass() = AuthenticationActivity::class.java

    @Before
    fun startRecordingIntents() = Intents.init()

    @After
    fun stopRecordingIntents() = Intents.release()

    @Test
    fun whenEnterValidPickupCode_thenOpenCorrespondingLockerBox() {
        onView(withId(R.id.et_pickup_code)).perform(replaceText(VALID_PICKUP_CODE), pressImeActionButton());
        assertOpenCorrespondLockerBox(VALID_PICKUP_CODE)
    }

    private fun assertOpenCorrespondLockerBox(pickupCode: String) {
        onView(withId(R.id.activity_user_open_locker_box)).check(matches(isDisplayed()))
        assertThat(getIntents().first()).hasComponentClass(UserOpenLockerBoxActivity::class.java)
        assertThat(getIntents().first()).extras()
                .integer(UserOpenLockerBoxActivity.EXTRA_LOCKER_BOX_NUMBER)
                .isEqualTo(PICKUP_CODE_TO_LOCKER_BOX[pickupCode])
    }

    @Test
    fun whenEnterNonDigitPickupCode_thenIgnoreIt() {
        val digitFreePickupCode = "Aq~.,;'/["
        onView(withId(R.id.et_pickup_code)).perform(typeText(digitFreePickupCode))
        assertIgnoringNonDigitInput()
    }

    private fun assertIgnoringNonDigitInput() {
        val nonIgnoredInput = getText(withId(R.id.et_pickup_code))
        assertEquals("", nonIgnoredInput)
    }

    @Test
    fun whenScanValidAuthCode_thenDisplayDriverOpenLockerBoxScreen() {
        simulateScanningAction(VALID_AUTH_CODE)
        assertDisplayingDriverOpenLockerBoxScreen()
    }

    private fun assertDisplayingDriverOpenLockerBoxScreen() {
        onView(withId(R.id.activity_driver_open_locker_box)).check(matches(isDisplayed()))
        assertThat(getIntents().first()).hasComponentClass(DriverOpenLockerBoxActivity::class.java)
    }

    @Test
    fun whenScanInvalidAuthCode_thenDisplayErrorMessage() {
        simulateScanningAction(INVALID_AUTH_CODE)
        onView(withId(R.id.dialog_message)).check(matches(isDisplayed()))
    }

    @Test
    fun whenScanAuthCodesWhileScreenIsLoading_thenIgnoreThem() {
        simulateScanningAction(INVALID_AUTH_CODE) // screen starts loading on scanning
        for (i in 1..SCAN_ATTEMPTS_COUNT) simulateScanningAction(VALID_AUTH_CODE)
        onView(withId(R.id.dialog_message)).check(matches(isDisplayed())) // assert only the 1st code was considered
    }
}