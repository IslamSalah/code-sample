package com.noon.lockers.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.getIntents
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.truth.content.IntentSubject.assertThat
import com.noon.lockers.R
import com.noon.lockers.ui.authentication.AuthenticationActivity
import com.noon.lockers.ui.base.BaseScreenTest
import com.noon.lockers.ui.welcome.WelcomeActivity
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Islam Salah on 2019-05-02.
 *
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * Add UI tests for home welcome screen
 */
@RunWith(AndroidJUnit4::class)
class WelcomeScreenTest : BaseScreenTest<WelcomeActivity>() {

    override fun getActivityClass() = WelcomeActivity::class.java

    @Before
    fun startRecordingIntents() = Intents.init()

    @After
    fun stopRecordingIntents() = Intents.release()

    @Test
    fun whenTouchScreen_thenShouldLaunchAuthenticationScreen() {
        onView(withId(R.id.cl_root_view)).perform(click())
        assertThat(getIntents().first()).hasComponentClass(AuthenticationActivity::class.java)
    }
}