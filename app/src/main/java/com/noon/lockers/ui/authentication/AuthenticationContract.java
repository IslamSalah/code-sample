package com.noon.lockers.ui.authentication;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface AuthenticationContract {

    interface View extends BaseView {

        void showKeyboard();

        void showUserOpenLockerBoxScreen(int lockerBoxNumber);

        void showDriverOpenLockerBoxScreen(String accessToken, boolean isPutAwayRole);

        void dispatchUserInteraction();
    }

    interface Presenter extends BasePresenter<View> {

        void authenticatePickupCode(String pickupCode);
    }
}
