package com.noon.lockers.ui.authentication;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.LockerBox;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.ui.base.presenter.ScanningPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class AuthenticationPresenterImp extends ScanningPresenter<AuthenticationContract.View>
        implements AuthenticationContract.Presenter {

    @Inject
    public AuthenticationPresenterImp(DataManager dataManager, ErrorHandler errorHandler,
                                      LockerMachine lockerMachine) {
        super(dataManager, errorHandler, lockerMachine);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getView().showKeyboard();
    }

    @Override
    protected void onPause() {
        getView().hideKeyboard();
        super.onPause();
    }

    @Override
    public void authenticatePickupCode(String pickupCode) {
        addDisposable(getDataManager()
                .authenticatePickupCode(pickupCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(LockerBox::getBoxNumber)
                .doOnSubscribe(disposable -> {
                    getView().showLoading();
                    getLockerMachine().turnOnScanner(false);
                }).doOnTerminate(getView()::hideLoading)
                .doFinally(() -> getLockerMachine().turnOnScanner(true))
                .subscribe(boxNumber -> getView().showUserOpenLockerBoxScreen(boxNumber)
                        , this::showErrorMessage));
    }

    private void authenticateDriverQrCode(String code) {
        addDisposable(getDataManager()
                .authenticateDriverQrCode(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    runOnUiThread(getView()::showLoading);
                    getLockerMachine().turnOnScanner(false);
                }).doOnTerminate(getView()::hideLoading)
                .doFinally(() -> getLockerMachine().turnOnScanner(true))
                .subscribe(this::showDriverOpenLockerBoxScreen, this::showErrorMessage));
    }

    private void showDriverOpenLockerBoxScreen(Driver driver) {
        String accessToken = driver.getAccessToken();
        boolean isPutAwayRole = driver.isPutAwayRole();
        getView().showDriverOpenLockerBoxScreen(accessToken, isPutAwayRole);
    }

    @Override
    protected void onCodeScanned(String scannedCode) {
        getView().dispatchUserInteraction();
        authenticateDriverQrCode(scannedCode);
    }
}
