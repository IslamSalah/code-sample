package com.noon.lockers.ui.base.presenter;

import com.noon.lockers.ui.base.view.BaseView;

import androidx.lifecycle.LifecycleObserver;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface BasePresenter<V extends BaseView> extends LifecycleObserver {

    void attachView(V view);

    void detachView();
}
