package com.noon.lockers.ui.registration;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 4/7/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface RegistrationContract {

    interface View extends BaseView {

        void showWelcomeScreen();

        void showAppVersion();
    }

    interface Presenter extends BasePresenter<View> {

    }
}
