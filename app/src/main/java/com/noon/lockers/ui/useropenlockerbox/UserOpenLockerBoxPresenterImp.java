package com.noon.lockers.ui.useropenlockerbox;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.ui.base.presenter.BasePresenterImp;

import javax.inject.Inject;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class UserOpenLockerBoxPresenterImp extends BasePresenterImp<UserOpenLockerBoxContract.View>
        implements UserOpenLockerBoxContract.Presenter {

    private LockerMachine mLockerMachine;

    @Inject
    public UserOpenLockerBoxPresenterImp(DataManager dataManager, ErrorHandler errorHandler,
                                         LockerMachine lockerMachine) {
        super(dataManager, errorHandler);
        mLockerMachine = lockerMachine;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        openLockerBox();
        getView().showOpenedBoxNumber();
    }

    private void openLockerBox() {
        int boxNumber = getView().getLockerBoxNumber();
        mLockerMachine.openBox(boxNumber);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private void onResume() {
        mLockerMachine.setOnCloseBoxListener(closedBoxNumber -> {
            int openedBoxNumber = getView().getLockerBoxNumber();
            if (openedBoxNumber == closedBoxNumber) {
                getView().showThankYouScreen();
            }
        });
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private void onPause() {
        mLockerMachine.setOnCloseBoxListener(null);
    }
}
