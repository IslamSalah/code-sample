package com.noon.lockers.ui.authentication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.noon.lockers.R;
import com.noon.lockers.ui.base.view.DetectIdleUserActivity;
import com.noon.lockers.ui.driveropenlockerbox.DriverOpenLockerBoxActivity;
import com.noon.lockers.ui.useropenlockerbox.UserOpenLockerBoxActivity;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class AuthenticationActivity extends DetectIdleUserActivity
        implements AuthenticationContract.View {

    @BindView(R.id.tv_instruction)
    AppCompatTextView mInstructionTv;

    @BindView(R.id.et_pickup_code)
    TextInputEditText mPickupCodeEt;

    @Inject
    AuthenticationContract.Presenter mPresenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, AuthenticationActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePickupCodeView();
    }

    private void initializePickupCodeView() {
        setOnEnterPickupCodeListener();
        disableActionsOnSelectedText();
        disablePickupCodeMovingCursor();
        showEnteredPickupCode();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_authentication;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    private void setOnEnterPickupCodeListener() {
        mPickupCodeEt.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (!isEnterKey(actionId, keyEvent)) return false;
            String pickupCode = mPickupCodeEt.getText().toString();
            mPresenter.authenticatePickupCode(pickupCode);
            return true;
        });
    }

    private boolean isEnterKey(int actionId, KeyEvent keyEvent) {
        return isSoftKeyboardEnterKey(actionId) || isHardKeyboardEnterKey(keyEvent);
    }

    private boolean isSoftKeyboardEnterKey(int actionId) {
        return actionId == EditorInfo.IME_ACTION_DONE;
    }

    private boolean isHardKeyboardEnterKey(KeyEvent keyEvent) {
        return keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER
                && keyEvent.getAction() == KeyEvent.ACTION_DOWN;
    }

    @Override
    public void showKeyboard() {
        mPickupCodeEt.requestFocus();
        InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void disableActionsOnSelectedText() {
        mPickupCodeEt.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }
        });
    }

    private void disablePickupCodeMovingCursor() {
        // Consume cursor moving keys
        mPickupCodeEt.setOnKeyListener((view, keyCode, keyEvent) -> isCursorMovingKey(keyEvent));
        mPickupCodeEt.setOnClickListener(view -> {
            int lastIndex = mPickupCodeEt.getText() == null ? 0 : mPickupCodeEt.length();
            mPickupCodeEt.setSelection(lastIndex);
        });
    }

    private boolean isCursorMovingKey(KeyEvent keyEvent) {
        return keyEvent.getKeyCode() == KeyEvent.KEYCODE_PAGE_UP
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_PAGE_DOWN;
    }

    private void showEnteredPickupCode() {
        mPickupCodeEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
    }

    @Override
    public void showUserOpenLockerBoxScreen(int lockerBoxNumber) {
        Intent intent = UserOpenLockerBoxActivity.newIntent(this, lockerBoxNumber);
        startActivity(intent);
    }

    @Override
    public void showDriverOpenLockerBoxScreen(String accessToken, boolean isPutAwayRole) {
        Intent intent = DriverOpenLockerBoxActivity.newIntent(this, accessToken, isPutAwayRole);
        startActivity(intent);
    }

    @Override
    public void dispatchUserInteraction() {
        restartIdleTimer();
    }
}
