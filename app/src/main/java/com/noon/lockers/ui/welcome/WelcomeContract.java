package com.noon.lockers.ui.welcome;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface WelcomeContract {

    interface View extends BaseView {

        void showWelcomeMessage(String lockerName);

        void showAppVersion();

        void showAuthenticationScreen();
    }

    interface Presenter extends BasePresenter<View> {

        void authenticateUser();
    }
}
