package com.noon.lockers.ui.thankyou;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.ui.base.presenter.BasePresenterImp;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ThankYouPresenterImp extends BasePresenterImp<ThankYouContract.View>
        implements ThankYouContract.Presenter {

    private static final int LIVE_TIME_PERIOD = 3; // seconds

    @Inject
    public ThankYouPresenterImp(DataManager dataManager, ErrorHandler errorHandler) {
        super(dataManager, errorHandler);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        switchViewOnLifeTimeExpiration();
    }

    private void switchViewOnLifeTimeExpiration() {
        addDisposable(Completable.complete()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .delay(LIVE_TIME_PERIOD, TimeUnit.SECONDS)
                .subscribe(getView()::showWelcomeScreen));
    }

    @Override
    public void welcomeUser() {
        getView().showWelcomeScreen();
    }
}
