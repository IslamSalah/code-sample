package com.noon.lockers.ui.driveropenlockerbox;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.noon.lockers.R;
import com.noon.lockers.ui.base.view.DetectIdleUserActivity;
import com.noon.lockers.ui.thankyou.ThankYouActivity;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Islam Salah on 3/30/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class DriverOpenLockerBoxActivity extends DetectIdleUserActivity
        implements DriverOpenLockerBoxContract.View {

    private static final String EXTRA_DRIVER_ACCESS_TOKEN = "EXTRA_DRIVER_ACCESS_TOKEN";
    private static final String EXTRA_DRIVER_ACCESS_ROLE = "EXTRA_DRIVER_ACCESS_ROLE";

    @BindView(R.id.tv_package_type)
    AppCompatTextView mPackageTypeTv;

    @Inject
    DriverOpenLockerBoxContract.Presenter mPresenter;

    private String mDriverAccessToken;
    private boolean mIsPutAwayRole;

    public static Intent newIntent(Context context, String accessToken, boolean isPutAwayRole) {
        return new Intent(context, DriverOpenLockerBoxActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(EXTRA_DRIVER_ACCESS_TOKEN, accessToken)
                .putExtra(EXTRA_DRIVER_ACCESS_ROLE, isPutAwayRole);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractExtras();
    }

    private void extractExtras() {
        mDriverAccessToken = getIntent().getStringExtra(EXTRA_DRIVER_ACCESS_TOKEN);
        mIsPutAwayRole = getIntent().getBooleanExtra(EXTRA_DRIVER_ACCESS_ROLE, false);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_driver_open_locker_box;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    @OnClick(R.id.tv_check_out)
    void checkout() {
        mPresenter.checkout();
    }

    @Override
    public void showDriverRole() {
        mPackageTypeTv.setText(mIsPutAwayRole ? R.string.put_away_message : R.string.harvest_message);
    }

    @Override
    public void showThankYouScreen() {
        Intent intent = ThankYouActivity.newIntent(this);
        startActivity(intent);
    }

    @Override
    public void dispatchUserInteraction() {
        restartIdleTimer();
    }

    @Override
    public String getDriverAccessToken() {
        return mDriverAccessToken;
    }

    @Override
    public boolean shouldDriverPutAway() {
        return mIsPutAwayRole;
    }

    @Override
    public void showOpenedBoxError(int boxNumber) {
        String boxName = String.valueOf(boxNumber);
        String errorMessage = getString(R.string.opened_box_error_message, boxName);
        showMessage(errorMessage);
    }
}
