package com.noon.lockers.ui.welcome;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.ui.base.presenter.BasePresenterImp;

import javax.inject.Inject;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class WelcomePresenterImp extends BasePresenterImp<WelcomeContract.View>
        implements WelcomeContract.Presenter {

    @Inject
    public WelcomePresenterImp(DataManager dataManager, ErrorHandler errorHandler) {
        super(dataManager, errorHandler);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        String lockerName = getCapitalizedName();
        getView().showWelcomeMessage(lockerName);
        getView().showAppVersion();
    }

    private String getCapitalizedName() {
        String name = getDataManager().getLockerName();
        return capitalize(name);
    }

    private String capitalize(String word) {
        if (word == null || word.length() == 0) return null;

        String firstLetter = word.substring(0, 1).toUpperCase();
        return firstLetter + word.substring(1);
    }

    @Override
    public void authenticateUser() {
        getView().showAuthenticationScreen();
    }
}
