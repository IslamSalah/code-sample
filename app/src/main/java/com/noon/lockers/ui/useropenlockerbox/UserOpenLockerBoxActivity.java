package com.noon.lockers.ui.useropenlockerbox;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.widget.AppCompatTextView;

import com.noon.lockers.R;
import com.noon.lockers.ui.base.view.DetectIdleUserActivity;
import com.noon.lockers.ui.thankyou.ThankYouActivity;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

public class UserOpenLockerBoxActivity extends DetectIdleUserActivity
        implements UserOpenLockerBoxContract.View {

    @VisibleForTesting
    public static final String EXTRA_LOCKER_BOX_NUMBER = "EXTRA_LOCKER_BOX_NUMBER";

    @BindView(R.id.tv_locker_box_number)
    AppCompatTextView mLockerBoxNumberTv;

    @Inject
    UserOpenLockerBoxContract.Presenter mPresenter;

    private int mLockerBoxNumber;

    public static Intent newIntent(Context context, int boxNumber) {
        return new Intent(context, UserOpenLockerBoxActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(EXTRA_LOCKER_BOX_NUMBER, boxNumber);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractExtras();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    private void extractExtras() {
        mLockerBoxNumber = getIntent().getIntExtra(EXTRA_LOCKER_BOX_NUMBER, 0);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_user_open_locker_box;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);

    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    @Override
    public int getLockerBoxNumber() {
        return mLockerBoxNumber;
    }

    @Override
    public void showOpenedBoxNumber() {
        String boxNumber = String.valueOf(mLockerBoxNumber);
        mLockerBoxNumberTv.setText(boxNumber);
    }

    @Override
    public void showThankYouScreen() {
        Intent intent = ThankYouActivity.newIntent(this);
        startActivity(intent);
    }
}
