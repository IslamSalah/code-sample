package com.noon.lockers.ui.useropenlockerbox;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface UserOpenLockerBoxContract {

    interface View extends BaseView {

        int getLockerBoxNumber();

        void showOpenedBoxNumber();

        void showThankYouScreen();
    }

    interface Presenter extends BasePresenter<View> {

    }
}
