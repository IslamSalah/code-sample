package com.noon.lockers.ui.registration;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.ui.base.presenter.ScanningPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Islam Salah on 4/7/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class RegistrationPresenterImp extends ScanningPresenter<RegistrationContract.View>
        implements RegistrationContract.Presenter {

    @Inject
    public RegistrationPresenterImp(DataManager dataManager, ErrorHandler errorHandler,
                                    LockerMachine lockerMachine) {
        super(dataManager, errorHandler, lockerMachine);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        getView().showAppVersion();
        if (isLockerRegistered()) getView().showWelcomeScreen();
    }

    private boolean isLockerRegistered() {
        return getDataManager().getLockerId() != null;
    }

    @Override
    protected void onCodeScanned(String registrationQrCode) {
        addDisposable(getDataManager()
                .registerLocker(registrationQrCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    runOnUiThread(getView()::showLoading);
                    getLockerMachine().turnOnScanner(false);
                }).doOnTerminate(getView()::hideLoading)
                .doFinally(() -> getLockerMachine().turnOnScanner(true))
                .subscribe(locker -> {
                    persistLockerInformation(locker);
                    getView().showWelcomeScreen();
                }, this::showErrorMessage));
    }

    private void persistLockerInformation(Locker locker) {
        getDataManager().setLockerId(locker.getId());
        getDataManager().setLockerName(locker.getEnglishName());
    }
}
