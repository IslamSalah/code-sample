package com.noon.lockers.ui.driveropenlockerbox;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 3/30/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface DriverOpenLockerBoxContract {

    interface View extends BaseView {

        void showDriverRole();

        void showThankYouScreen();

        void dispatchUserInteraction();

        String getDriverAccessToken();

        boolean shouldDriverPutAway();

        void showOpenedBoxError(int boxNumber);
    }

    interface Presenter extends BasePresenter<View> {

        void checkout();
    }
}
