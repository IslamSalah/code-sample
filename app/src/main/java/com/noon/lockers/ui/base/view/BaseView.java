package com.noon.lockers.ui.base.view;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface BaseView {

    void showLoading();

    void hideLoading();

    void showMessage(String message);

    void hideMessage();

    void hideKeyboard();
}
