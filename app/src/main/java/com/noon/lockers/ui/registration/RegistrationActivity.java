package com.noon.lockers.ui.registration;

import android.content.Context;
import android.content.Intent;

import com.noon.lockers.BuildConfig;
import com.noon.lockers.R;
import com.noon.lockers.ui.base.view.BaseActivity;
import com.noon.lockers.ui.welcome.WelcomeActivity;

import javax.inject.Inject;

import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;

/**
 * Created by Islam Salah on 4/7/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class RegistrationActivity extends BaseActivity implements
        RegistrationContract.View {

    @BindView(R.id.tv_app_version)
    AppCompatTextView mAppVersionTv;

    @Inject
    RegistrationContract.Presenter mPresenter;

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_registration_locker;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    @Override
    public void showWelcomeScreen() {
        Intent intent = WelcomeActivity.newIntent(this);
        startActivity(intent);
    }

    @Override
    public void showAppVersion() {
        mAppVersionTv.setText(BuildConfig.VERSION_NAME);
    }
}
