package com.noon.lockers.ui.welcome;

import android.content.Context;
import android.content.Intent;

import com.noon.lockers.BuildConfig;
import com.noon.lockers.R;
import com.noon.lockers.ui.authentication.AuthenticationActivity;
import com.noon.lockers.ui.base.view.BaseActivity;

import javax.inject.Inject;

import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity implements WelcomeContract.View {

    @BindView(R.id.tv_welcome)
    AppCompatTextView mWelcomeTv;

    @BindView(R.id.tv_app_version)
    AppCompatTextView mAppVersionTv;

    @Inject
    WelcomeContract.Presenter mPresenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, WelcomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    @OnClick(R.id.cl_root_view)
    public void touchScreen() {
        mPresenter.authenticateUser();
    }

    @Override
    public void showWelcomeMessage(String lockerName) {
        mWelcomeTv.setText(getString(R.string.welcome_message, lockerName));
    }

    @Override
    public void showAppVersion() {
        mAppVersionTv.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void showAuthenticationScreen() {
        Intent intent = AuthenticationActivity.newIntent(this);
        startActivity(intent);
    }
}
