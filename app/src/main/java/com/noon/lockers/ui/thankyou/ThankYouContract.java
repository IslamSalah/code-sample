package com.noon.lockers.ui.thankyou;

import com.noon.lockers.ui.base.presenter.BasePresenter;
import com.noon.lockers.ui.base.view.BaseView;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface ThankYouContract {

    interface View extends BaseView {

        void showWelcomeScreen();
    }

    interface Presenter extends BasePresenter<View> {

        void welcomeUser();
    }
}
