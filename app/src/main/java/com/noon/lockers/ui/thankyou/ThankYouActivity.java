package com.noon.lockers.ui.thankyou;

import android.content.Context;
import android.content.Intent;

import com.noon.lockers.R;
import com.noon.lockers.ui.base.view.BaseActivity;
import com.noon.lockers.ui.welcome.WelcomeActivity;

import javax.inject.Inject;

import butterknife.OnClick;

/**
 * Created by Islam Salah on 3/28/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ThankYouActivity extends BaseActivity
        implements ThankYouContract.View {

    @Inject
    ThankYouContract.Presenter mPresenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, ThankYouActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_thank_you;
    }

    @Override
    protected void injectPresenter() {
        getActivityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void addLifecycleObservers() {
        getLifecycle().addObserver(mPresenter);
    }

    @OnClick(R.id.cl_root_view)
    public void touchScreen() {
        mPresenter.welcomeUser();
    }

    @Override
    public void showWelcomeScreen() {
        Intent intent = WelcomeActivity.newIntent(this);
        startActivity(intent);
    }
}
