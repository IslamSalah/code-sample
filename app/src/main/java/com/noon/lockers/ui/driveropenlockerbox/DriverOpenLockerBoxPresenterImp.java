package com.noon.lockers.ui.driveropenlockerbox;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.ui.base.presenter.ScanningPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Islam Salah on 3/30/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class DriverOpenLockerBoxPresenterImp extends ScanningPresenter<DriverOpenLockerBoxContract.View>
        implements DriverOpenLockerBoxContract.Presenter {

    private static final int INVALID_BOX_NUMBER = -1;

    private int mOpenedBoxNumber = INVALID_BOX_NUMBER;

    @Inject
    public DriverOpenLockerBoxPresenterImp(DataManager dataManager, ErrorHandler errorHandler,
                                           LockerMachine lockerMachine) {
        super(dataManager, errorHandler, lockerMachine);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        getView().showDriverRole();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLockerMachine().setOnCloseBoxListener(this::notifyClosingBox);
    }

    private void notifyClosingBox(int boxNumber) {
        if (mOpenedBoxNumber == boxNumber) {
            clearOpenedBoxNumber();
            getView().hideMessage();
        }
    }

    private void clearOpenedBoxNumber() {
        mOpenedBoxNumber = INVALID_BOX_NUMBER;
    }

    @Override
    protected void onPause() {
        getLockerMachine().setOnCloseBoxListener(null);
        super.onPause();
    }

    @Override
    protected void onCodeScanned(String scannedCode) {
        getView().dispatchUserInteraction();
        if (hasClosedLastOpenedBox()) {
            scanPackageQrCode(scannedCode);
        } else {
            runOnUiThread(() -> getView().showOpenedBoxError(mOpenedBoxNumber));
        }
    }

    private boolean hasClosedLastOpenedBox() {
        return mOpenedBoxNumber == INVALID_BOX_NUMBER;
    }

    private void scanPackageQrCode(String packageQrCode) {
        String driverAccessToken = getView().getDriverAccessToken();
        boolean isPutAwayAction = getView().shouldDriverPutAway();
        scanPackageQrCode(driverAccessToken, packageQrCode, isPutAwayAction);
    }

    private void scanPackageQrCode(String driverAccessToken, String packageQrCode, boolean isPutAwayAction) {
        addDisposable(getDataManager()
                .scanPackageQrCode(driverAccessToken, packageQrCode, isPutAwayAction)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    runOnUiThread(getView()::showLoading);
                    getLockerMachine().turnOnScanner(false);
                }).doOnTerminate(getView()::hideLoading)
                .doFinally(() -> getLockerMachine().turnOnScanner(true))
                .subscribe(packageResponse -> openLockerBox(packageResponse.getBoxNumber())
                        , this::showErrorMessage));
    }

    private void openLockerBox(int boxNumber) {
        mOpenedBoxNumber = boxNumber;
        getLockerMachine().openBox(boxNumber);
    }

    @Override
    public void checkout() {
        getView().showThankYouScreen();
    }
}
