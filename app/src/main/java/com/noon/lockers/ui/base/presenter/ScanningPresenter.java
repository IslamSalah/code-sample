package com.noon.lockers.ui.base.presenter;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.ui.base.view.BaseView;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Islam Salah on 4/10/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public abstract class ScanningPresenter<V extends BaseView> extends BasePresenterImp<V> {

    private LockerMachine mLockerMachine;

    public ScanningPresenter(DataManager dataManager, ErrorHandler errorHandler,
                             LockerMachine lockerMachine) {
        super(dataManager, errorHandler);
        mLockerMachine = lockerMachine;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void onResume() {
        mLockerMachine.setOnScanListener(this::onCodeScanned);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected void onPause() {
        mLockerMachine.setOnScanListener(null);
    }

    public LockerMachine getLockerMachine() {
        return mLockerMachine;
    }

    // Calls to this method are triggered on a background thread
    protected abstract void onCodeScanned(String scannedCode);

    protected void runOnUiThread(Runnable runnable) {
        addDisposable(Completable.complete()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(runnable::run));
    }
}
