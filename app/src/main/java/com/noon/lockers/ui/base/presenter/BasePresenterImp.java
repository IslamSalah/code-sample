package com.noon.lockers.ui.base.presenter;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.ui.base.view.BaseView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class BasePresenterImp<V extends BaseView> implements BasePresenter<V> {

    private V mView;
    private final DataManager mDataManager;
    private final ErrorHandler mErrorHandler;
    private final CompositeDisposable mCompositeDisposable;

    public BasePresenterImp(DataManager dataManager, ErrorHandler errorHandler) {
        mDataManager = dataManager;
        mErrorHandler = errorHandler;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(V view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mCompositeDisposable.clear();
        mView = null;
    }

    protected V getView() {
        return mView;
    }

    protected DataManager getDataManager() {
        return mDataManager;
    }

    protected void showErrorMessage(Throwable throwable) throws Exception {
        String error = mErrorHandler.extractError(throwable);
        if (getView() != null) getView().showMessage(error);
    }

    protected void addDisposable(Disposable disposable){
        mCompositeDisposable.add(disposable);
    }
}
