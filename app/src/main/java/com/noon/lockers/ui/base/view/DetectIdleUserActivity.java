package com.noon.lockers.ui.base.view;

import android.content.Intent;
import android.os.Handler;

import com.noon.lockers.R;
import com.noon.lockers.ui.welcome.WelcomeActivity;

import butterknife.BindInt;

/**
 * Created by Islam Salah on 4/1/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public abstract class DetectIdleUserActivity extends BaseActivity {

    @BindInt(R.integer.idle_period_threshold)
    protected int mIdlePeriodThreshold;

    private final Handler mHandler = new Handler();
    private final Runnable mOnIdleTimeOutAction = this::showWelcomeScreen;

    @Override
    protected void onResume() {
        super.onResume();
        startIdleTimer();
    }

    @Override
    protected void onPause() {
        stopIdleTimer();
        super.onPause();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        restartIdleTimer();
    }

    private void startIdleTimer() {
        mHandler.postDelayed(mOnIdleTimeOutAction, mIdlePeriodThreshold);
    }

    private void stopIdleTimer() {
        mHandler.removeCallbacks(mOnIdleTimeOutAction);
    }

    protected void restartIdleTimer() {
        stopIdleTimer();
        startIdleTimer();
    }

    private void showWelcomeScreen() {
        Intent intent = WelcomeActivity.newIntent(this);
        startActivity(intent);
    }
}
