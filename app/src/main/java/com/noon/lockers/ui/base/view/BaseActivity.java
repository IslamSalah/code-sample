package com.noon.lockers.ui.base.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.noon.lockers.LockersApplication;
import com.noon.lockers.R;
import com.noon.lockers.di.component.ActivityComponent;
import com.noon.lockers.di.component.ApplicationComponent;
import com.noon.lockers.di.component.DaggerActivityComponent;
import com.noon.lockers.di.module.ActivityModule;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import butterknife.ButterKnife;
import icepick.Icepick;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private AlertDialog mMessageDialog;
    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeLayout();
        Icepick.restoreInstanceState(this, savedInstanceState);
        injectDependencies();
        addLifecycleObservers();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override // Disable keyboard key presses
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    private void initializeLayout() {
        setContentView(getLayout());
        ButterKnife.bind(this);
        hideAndroidBars();
    }

    @LayoutRes
    protected abstract int getLayout();

    private void injectDependencies() {
        initializeActivityComponent();
        injectPresenter();
    }

    private void initializeActivityComponent() {
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule())
                .applicationComponent(getApplicationComponent())
                .build();
    }

    private ApplicationComponent getApplicationComponent() {
        return ((LockersApplication) getApplication()).getApplicationComponent();
    }

    protected abstract void injectPresenter();

    protected abstract void addLifecycleObservers();

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    private void hideAndroidBars() {
        hideStatusBar();
        hideActionBar();
    }

    private void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    @Override
    public void showLoading() {
        hideDialog(mMessageDialog);
        showNewProgressDialog();
    }

    private void showNewProgressDialog() {
        mMessageDialog = showNewDialog();
        mMessageDialog.setContentView(R.layout.dialog_loading);
        mMessageDialog.setCancelable(false);
        mMessageDialog.setCanceledOnTouchOutside(false);
    }

    private AlertDialog showNewDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).show();
        if (dialog != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        return dialog;
    }

    @Override
    public void hideLoading() {
        hideDialog(mMessageDialog);
    }

    private void hideDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }

    @Override
    public void showMessage(String message) {
        hideDialog(mMessageDialog);
        mMessageDialog = showNewDialog();
        mMessageDialog.setContentView(R.layout.dialog_message);
        ((AppCompatTextView) mMessageDialog.findViewById(R.id.tv_message)).setText(message);
    }

    @Override
    public void hideMessage() {
        hideDialog(mMessageDialog);
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager manager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        IBinder windowToken = getCurrentFocus().getWindowToken();
        if (windowToken == null) return;
        manager.hideSoftInputFromWindow(windowToken, 0);
    }
}
