package com.noon.lockers.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Islam Salah on 4/4/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class StartupReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = context.getPackageName();
        Intent lauchingIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        context.startActivity(lauchingIntent);
    }
}
