package com.noon.lockers.data.network.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class Driver {

    private static final String PUT_AWAY_ROLE = "PUT_AWAY_ROLE";

    @SerializedName("param")
    private String mId;

    @SerializedName("param")
    private String mAccessRole;

    @SerializedName("param")
    private String mAccessToken;

    @SerializedName("param")
    private String mAccessTokenExpirationDate;

    public Driver(String id, String accessRole, String accessToken,
                  String accessTokenExpirationDate) {
        mId = id;
        mAccessRole = accessRole;
        mAccessToken = accessToken;
        mAccessTokenExpirationDate = accessTokenExpirationDate;
    }

    public String getId() {
        return mId;
    }

    public String getAccessRole() {
        return mAccessRole;
    }

    public boolean isPutAwayRole() {
        return PUT_AWAY_ROLE.equals(mAccessRole);
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public String getAccessTokenExpirationDate() {
        return mAccessTokenExpirationDate;
    }
}
