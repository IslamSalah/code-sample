package com.noon.lockers.data;

/**
 * Created by Islam Salah on 4/10/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * This class is supposed to handle errors generally
 */
public interface ErrorHandler {

    String extractError(Throwable throwable) throws Exception;
}
