package com.noon.lockers.data.network.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/10/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ApiError {

    @SerializedName("param")
    private String mError;

    public String getError() {
        return mError;
    }
}
