package com.noon.lockers.data;

import com.google.gson.GsonBuilder;
import com.noon.lockers.data.network.model.response.ApiError;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.HttpException;

/**
 * Created by Islam Salah on 4/10/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class ErrorHandlerImp implements ErrorHandler {

    @Inject
    public ErrorHandlerImp() {
        /* Required by dagger2 framework */
    }

    @Override
    public String extractError(Throwable throwable) throws IOException {
        if (throwable instanceof HttpException) {
            return extractApiError(throwable);
        } else {
            return extractGeneralError(throwable);
        }
    }

    private String extractApiError(Throwable throwable) throws IOException {
        HttpException exception = (HttpException) throwable;
        String errorBody = exception.response().errorBody().string();
        ApiError apiError = convertFromJson(errorBody);
        return formatMessage(apiError.getError());
    }

    private String extractGeneralError(Throwable throwable) {
        return throwable.getMessage();
    }

    private ApiError convertFromJson(String jsonErrorBody) {
        return new GsonBuilder().create()
                .fromJson(jsonErrorBody, ApiError.class);

    }

    private String formatMessage(String message) {
        message = removeLastLetter(message);
        return capitalize(message);
    }

    private String removeLastLetter(String message) {
        int lastLetterIndex = message.length() - 1;
        return message.substring(0, lastLetterIndex);
    }

    private String capitalize(String statement) {
        if (statement == null) return null;

        String firstLetter = statement.substring(0, 1).toUpperCase();
        return firstLetter + statement.substring(1);
    }
}
