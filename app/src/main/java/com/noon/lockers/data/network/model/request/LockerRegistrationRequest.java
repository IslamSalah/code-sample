package com.noon.lockers.data.network.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class LockerRegistrationRequest {

    @SerializedName("param")
    private String mSecretKey;

    public LockerRegistrationRequest(String secretKey) {
        mSecretKey = secretKey;
    }
}
