package com.noon.lockers.data.preferences;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface PreferencesHelper {

    String getLockerId();

    void setLockerId(String lockerId);

    String getLockerName();

    void setLockerName(String name);
}
