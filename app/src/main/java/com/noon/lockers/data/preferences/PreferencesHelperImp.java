package com.noon.lockers.data.preferences;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

import javax.inject.Inject;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class PreferencesHelperImp implements PreferencesHelper {

    private static final String PREFERENCES_KEY_LOCKER_ID = "PREFERENCES_KEY_LOCKER_ID";
    private static final String PREFERENCES_KEY_LOCKER_NAME = "PREFERENCES_KEY_LOCKER_NAME";

    @Inject
    public PreferencesHelperImp(Context context) {
        Hawk.init(context).build();
    }

    @Override
    public String getLockerId() {
        return Hawk.get(PREFERENCES_KEY_LOCKER_ID);
    }

    @Override
    public void setLockerId(String lockerId) {
        Hawk.put(PREFERENCES_KEY_LOCKER_ID, lockerId);
    }

    @Override
    public String getLockerName() {
        return Hawk.get(PREFERENCES_KEY_LOCKER_NAME);
    }

    @Override
    public void setLockerName(String name) {
        Hawk.put(PREFERENCES_KEY_LOCKER_NAME, name);
    }
}
