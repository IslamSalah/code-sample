package com.noon.lockers.data;

import com.noon.lockers.data.network.ApiHelper;
import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.data.network.model.response.LockerBox;
import com.noon.lockers.data.preferences.PreferencesHelper;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class DataManagerImp implements DataManager {

    private final ApiHelper mApiHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManagerImp(ApiHelper apiHelper, PreferencesHelper preferencesHelper) {
        mApiHelper = apiHelper;
        mPreferencesHelper = preferencesHelper;
    }

    @Override
    public Single<LockerBox> authenticatePickupCode(String pickupCode) {
        String lockerId = getLockerId();
        return authenticatePickupCode(lockerId, pickupCode);
    }

    @Override
    public Single<LockerBox> scanPackageQrCode(String driverAccessToken,
                                               String packageQrCode, boolean isPutAway) {
        String lockerId = getLockerId();
        return scanPackageQrCode(lockerId, driverAccessToken, packageQrCode, isPutAway);
    }

    @Override
    public Single<Locker> registerLocker(String secretKey) {
        return mApiHelper.registerLocker(secretKey);
    }

    @Override
    public Single<LockerBox> authenticatePickupCode(String lockerId, String pickupCode) {
        return mApiHelper.authenticatePickupCode(lockerId, pickupCode);
    }

    @Override
    public Single<Driver> authenticateDriverQrCode(String driverAccessToken) {
        return mApiHelper.authenticateDriverQrCode(driverAccessToken);
    }

    @Override
    public Single<LockerBox> scanPackageQrCode(String lockerId, String driverAccessToken,
                                               String packageQrCode, boolean isPutAway) {
        return mApiHelper.scanPackageQrCode(lockerId, driverAccessToken, packageQrCode, isPutAway);
    }

    @Override
    public String getLockerId() {
        return mPreferencesHelper.getLockerId();
    }

    @Override
    public void setLockerId(String lockerId) {
        mPreferencesHelper.setLockerId(lockerId);
    }

    @Override
    public String getLockerName() {
        return mPreferencesHelper.getLockerName();
    }

    @Override
    public void setLockerName(String name) {
        mPreferencesHelper.setLockerName(name);
    }
}
