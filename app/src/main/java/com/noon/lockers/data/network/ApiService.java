package com.noon.lockers.data.network;

import com.noon.lockers.data.network.model.request.LockerRegistrationRequest;
import com.noon.lockers.data.network.model.request.Package;
import com.noon.lockers.data.network.model.request.PickupRequest;
import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.data.network.model.response.LockerBox;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * This interface is supposed to act as the service consumed by retrofit
 */
public interface ApiService {

    String HEADER_LOCKER_ID = "HEADER_LOCKER_ID";
    String HEADER_DRIVER_ACCESS_TOKEN = "HEADER_DRIVER_ACCESS_TOKEN";

    @POST("endpoint")
    Single<Locker> registerLocker(@Body LockerRegistrationRequest request);

    @POST("endpoint")
    Single<LockerBox> pickupPackage(@Header(HEADER_LOCKER_ID) String lockerId,
                                    @Body PickupRequest request);

    @POST("endpoint")
    Single<Driver> authenticateDriver(@Header(HEADER_DRIVER_ACCESS_TOKEN) String driverAccessToken);

    @POST("endpoint")
    Single<LockerBox> harvest(@Header(HEADER_LOCKER_ID) String lockerId,
                              @Header(HEADER_DRIVER_ACCESS_TOKEN) String driverAccessToken,
                              @Body Package concernedPackage);

    @POST("endpoint")
    Single<LockerBox> putAway(@Header(HEADER_LOCKER_ID) String lockerId,
                              @Header(HEADER_DRIVER_ACCESS_TOKEN) String driverAccessToken,
                              @Body Package concernedPackage);
}
