package com.noon.lockers.data;

import com.noon.lockers.data.network.ApiHelper;
import com.noon.lockers.data.network.model.response.LockerBox;
import com.noon.lockers.data.preferences.PreferencesHelper;

import io.reactivex.Single;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface DataManager extends PreferencesHelper, ApiHelper {

    Single<LockerBox> authenticatePickupCode(String pickupCode);

    Single<LockerBox> scanPackageQrCode(String driverAccessToken,
                                        String packageQrCode, boolean isPutAway);
}
