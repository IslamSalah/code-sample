package com.noon.lockers.data.network.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class PickupRequest {

    @SerializedName("param")
    private String mPickupCode;

    @SerializedName("param")
    private String mTimestamp;      // format e.g 2019-04-07T12:38:15Z

    public PickupRequest(String pickupCode, String timestamp) {
        mPickupCode = pickupCode;
        mTimestamp = timestamp;
    }
}
