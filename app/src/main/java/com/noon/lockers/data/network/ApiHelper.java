package com.noon.lockers.data.network;

import com.noon.lockers.data.network.model.response.Driver;
import com.noon.lockers.data.network.model.response.Locker;
import com.noon.lockers.data.network.model.response.LockerBox;

import io.reactivex.Single;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface ApiHelper {

    Single<Locker> registerLocker(String secretKey);

    Single<LockerBox> authenticatePickupCode(String lockerId, String pickupCode);

    Single<Driver> authenticateDriverQrCode(String driverAccessToken);

    Single<LockerBox> scanPackageQrCode(String lockerId, String driverAccessToken,
                                        String packageQrCode, boolean isPutAway);
}
