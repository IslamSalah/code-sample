package com.noon.lockers.data.network.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class Locker {

    @SerializedName("param")
    private String mId;

    @SerializedName("param")
    private String mEnglishName;

    public Locker(String id, String englishName) {
        mId = id;
        mEnglishName = englishName;
    }

    public String getId() {
        return mId;
    }

    public String getEnglishName() {
        return mEnglishName;
    }
}
