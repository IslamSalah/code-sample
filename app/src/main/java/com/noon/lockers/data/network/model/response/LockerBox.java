package com.noon.lockers.data.network.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Islam Salah on 4/8/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class LockerBox {

    @SerializedName("param")
    private int mBoxNumber;   // Received numbers are 1-based

    public LockerBox(int boxNumber) {
        mBoxNumber = boxNumber;
    }

    public int getBoxNumber() {
        return mBoxNumber;
    }
}
