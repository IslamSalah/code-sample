package com.noon.lockers.locker;

import android.content.Context;

/**
 * Created by Islam Salah on 3/26/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public interface LockerMachine {

    boolean bind(Context context);

    void unBind(); // Unfortunately the library supports only unbinding the most recent bound component

    boolean openBox(int boxNumber);

    boolean isOpenedBox(int boxNumber);

    boolean isClosedBox(int boxNumber);

    // Allows turning the scanner on or off. By default the scanner is turned on
    void turnOnScanner(boolean isOn);

    // Calls to this listener are triggered on a worker thread
    void setOnScanListener(OnScanListener listener);

    // Calls to this listener are triggered on a worker thread
    void setOnCloseBoxListener(OnCloseBoxListener listener);

    interface OnScanListener {
        void onScan(String scannedCode);
    }

    interface OnCloseBoxListener {
        void onCloseBox(int boxNumber);
    }
}
