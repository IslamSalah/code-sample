package com.noon.lockers.locker;

import android.content.Context;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * Created by Islam Salah on 3/26/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */

/**
 * Dummy implementation of the locker machine
 */
public class LockerMachineImp implements LockerMachine {

    private OnCloseBoxListener mOnCloseBoxListener;
    private Set<Integer> mOpenedBoxesNumbers = new HashSet<>();

    private OnScanListener mOnScanListener;
    private boolean mIsScannerDisabled;

    @Inject
    public LockerMachineImp() {
        /* Required by dagger2 framework */
    }

    @Override
    public boolean bind(Context context) {
        initScanner();
        periodicallyNotifyCloseBoxListeners();
        return true; // dummy
    }

    private void initScanner() {
        // dummy
    }

    private void periodicallyNotifyCloseBoxListeners() {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                () -> attemptNotifyingCloseBoxListeners(),/*initialDelay*/ 0, /*period*/ 1, TimeUnit.MILLISECONDS);
    }

    /**
     * Prevents stopping the executor if the task throws and exception.
     *
     * @see ScheduledExecutorService#scheduleAtFixedRate
     */
    private void attemptNotifyingCloseBoxListeners() {
        try {
            notifyCloseBoxListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void notifyCloseBoxListeners() {
        Set<Integer> openedBoxesNumbersClone = new HashSet<>(mOpenedBoxesNumbers);
        for (int boxNumber : openedBoxesNumbersClone) {
            if (!isClosedBox(boxNumber)) continue;

            mOpenedBoxesNumbers.remove(boxNumber);
            if (mOnCloseBoxListener != null) mOnCloseBoxListener.onCloseBox(boxNumber);
        }
    }

    @Override
    public void unBind() {
    }

    @Override
    public boolean openBox(int boxNumber) {
        boolean isOperationSucceed = true; // dummy
        if (isOperationSucceed) mOpenedBoxesNumbers.add(boxNumber);
        return isOperationSucceed;
    }

    @Override
    public boolean isOpenedBox(int boxNumber) {
        return true; // dummy
    }

    @Override
    public boolean isClosedBox(int boxNumber) {
        return false; // dummy
    }

    @Override
    public void turnOnScanner(boolean isOn) {
        mIsScannerDisabled = !isOn;
    }

    /**
     * Scans QR and Bar codes
     */
    @Override
    public void setOnScanListener(OnScanListener listener) {
        mOnScanListener = listener;
    }

    @Override
    public void setOnCloseBoxListener(OnCloseBoxListener listener) {
        mOnCloseBoxListener = listener;
    }
}
