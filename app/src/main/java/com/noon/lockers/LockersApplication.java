package com.noon.lockers;


import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.noon.lockers.di.component.ApplicationComponent;
import com.noon.lockers.di.component.DaggerApplicationComponent;
import com.noon.lockers.di.module.ApplicationModule;
import com.noon.lockers.locker.LockerMachine;

import javax.inject.Inject;

import androidx.multidex.MultiDexApplication;
import io.fabric.sdk.android.Fabric;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
public class LockersApplication extends MultiDexApplication {

    private static final int NONZERO_STATUS_CODE = 1;

    @Inject
    LockerMachine mLockerMachine;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setDefaultUncaughtExceptionHandler();
        initializeMonitoringTools();
        initializeApplicationComponent();
        bindLockerMachineService();
    }


    /**
     * Relaunch the app anytime it crashes.
     * <p>
     * This handler should be set before initializing Crashlytics, otherwise it
     * will override CrashlyticsUncaughtExceptionHandler which will prevent Crashlytics
     * from working properly
     */
    private void setDefaultUncaughtExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            launchMainActivity();
            System.exit(NONZERO_STATUS_CODE);
        });
    }

    private void launchMainActivity() {
        Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        startActivity(intent);
    }

    private void initializeMonitoringTools() {
        Stetho.initializeWithDefaults(this);
        Fabric.with(this, new Crashlytics());
        RxJavaPlugins.setErrorHandler(Crashlytics::logException);
    }

    private void initializeApplicationComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void bindLockerMachineService() {
        mApplicationComponent.inject(this);
        mLockerMachine.bind(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    // To allow changing component with test component
    public void setApplicationComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
