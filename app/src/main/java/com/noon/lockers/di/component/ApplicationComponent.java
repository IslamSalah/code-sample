package com.noon.lockers.di.component;

import com.noon.lockers.LockersApplication;
import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.di.module.ApplicationModule;
import com.noon.lockers.locker.LockerMachine;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LockersApplication application);

    // Expose to sub graph
    ErrorHandler getErrorHandler();

    DataManager getDataManager();

    LockerMachine getLockerMachine();
}
