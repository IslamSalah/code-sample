package com.noon.lockers.di.module;

import android.app.Application;
import android.content.Context;

import com.noon.lockers.data.DataManager;
import com.noon.lockers.data.DataManagerImp;
import com.noon.lockers.data.ErrorHandler;
import com.noon.lockers.data.ErrorHandlerImp;
import com.noon.lockers.data.network.ApiHelper;
import com.noon.lockers.data.network.ApiHelperImp;
import com.noon.lockers.data.preferences.PreferencesHelper;
import com.noon.lockers.data.preferences.PreferencesHelperImp;
import com.noon.lockers.locker.LockerMachine;
import com.noon.lockers.locker.LockerMachineImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@Module(includes = ApiModule.class)
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(PreferencesHelperImp preferencesHelper) {
        return preferencesHelper;
    }

    @Provides
    @Singleton
    ErrorHandler provideErrorHandler(ErrorHandlerImp errorHandler) {
        return errorHandler;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(ApiHelperImp apiHelper) {
        return apiHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(DataManagerImp dataManager) {
        return dataManager;
    }

    @Provides
    @Singleton
    LockerMachine provideLockerMachine(LockerMachineImp lockerMachine) {
        return lockerMachine;
    }
}
