package com.noon.lockers.di.module;

import com.noon.lockers.di.PerActivity;
import com.noon.lockers.ui.authentication.AuthenticationContract;
import com.noon.lockers.ui.authentication.AuthenticationPresenterImp;
import com.noon.lockers.ui.driveropenlockerbox.DriverOpenLockerBoxContract;
import com.noon.lockers.ui.driveropenlockerbox.DriverOpenLockerBoxPresenterImp;
import com.noon.lockers.ui.registration.RegistrationContract;
import com.noon.lockers.ui.registration.RegistrationPresenterImp;
import com.noon.lockers.ui.thankyou.ThankYouContract;
import com.noon.lockers.ui.thankyou.ThankYouPresenterImp;
import com.noon.lockers.ui.useropenlockerbox.UserOpenLockerBoxContract;
import com.noon.lockers.ui.useropenlockerbox.UserOpenLockerBoxPresenterImp;
import com.noon.lockers.ui.welcome.WelcomeContract;
import com.noon.lockers.ui.welcome.WelcomePresenterImp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@Module
public class ActivityModule {

    @Provides
    @PerActivity
    WelcomeContract.Presenter provideWelcomePresenter(WelcomePresenterImp presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AuthenticationContract.Presenter provideAuthenticationPresenter(
            AuthenticationPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    UserOpenLockerBoxContract.Presenter provideUserOpenLockerBoxPresenter(
            UserOpenLockerBoxPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ThankYouContract.Presenter provideThankYouPresenter(ThankYouPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DriverOpenLockerBoxContract.Presenter provideDriverOpenLockerBoxPresenter(
            DriverOpenLockerBoxPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegistrationContract.Presenter provideRegistrationPresenter(
            RegistrationPresenterImp presenter) {
        return presenter;
    }
}
