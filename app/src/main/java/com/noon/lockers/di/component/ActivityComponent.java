package com.noon.lockers.di.component;

import com.noon.lockers.di.PerActivity;
import com.noon.lockers.di.module.ActivityModule;
import com.noon.lockers.ui.authentication.AuthenticationActivity;
import com.noon.lockers.ui.driveropenlockerbox.DriverOpenLockerBoxActivity;
import com.noon.lockers.ui.registration.RegistrationActivity;
import com.noon.lockers.ui.thankyou.ThankYouActivity;
import com.noon.lockers.ui.useropenlockerbox.UserOpenLockerBoxActivity;
import com.noon.lockers.ui.welcome.WelcomeActivity;

import dagger.Component;

/**
 * Created by Islam Salah on 3/27/19.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@PerActivity
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(WelcomeActivity activity);

    void inject(AuthenticationActivity activity);

    void inject(UserOpenLockerBoxActivity activity);

    void inject(ThankYouActivity activity);

    void inject(DriverOpenLockerBoxActivity activity);

    void inject(RegistrationActivity activity);
}
